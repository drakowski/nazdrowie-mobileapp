package www.restauracjanazdrowie.myapplication.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.text.Layout.JUSTIFICATION_MODE_INTER_WORD
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.viewpager.widget.PagerAdapter
import www.restauracjanazdrowie.myapplication.R
import www.restauracjanazdrowie.myapplication.model.Article

class StartDataViewPagerAdapter(private val mContext : Context, private val itemList: ArrayList<Article>) : PagerAdapter() {

    private var layoutInflater: LayoutInflater? = null

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("UseCompatLoadingForDrawables", "WrongConstant")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = LayoutInflater.from(mContext)
        val view = layoutInflater!!.inflate(R.layout.article, container, false)


        val cardView : CardView = view.findViewById(R.id.articleCardView);
        val imageView = ImageView(mContext);
        val currentItem : Article = itemList.get(position);

        val layout2 = LinearLayout(mContext)
        layout2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layout2.orientation = LinearLayout.VERTICAL

        val params2: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 700);

        imageView.setImageDrawable(mContext.getDrawable(currentItem.getResourceId()));
        imageView.scaleType = ImageView.ScaleType.FIT_XY;
        imageView.layoutParams = params2;

        val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        val textView = TextView(mContext)
        textView.setText(currentItem.getTitle());
        textView.setLayoutParams(params);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        textView.setTextSize(18f);
        layout2.addView(imageView);
        layout2.addView(textView);

        val textViewDesc = TextView(mContext);
        textViewDesc.setText(currentItem.getDesc());
        textViewDesc.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        textViewDesc.setPadding(30,30,30,30);
        layout2.addView(textViewDesc);

        cardView.setPreventCornerOverlap(false);

        val scrollView : ScrollView = view.findViewById(R.id.articleScrollView);
        scrollView.addView(layout2);

        container.addView(view, position);

        return view
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view === obj;
    }

    override fun getCount(): Int {
        return itemList.size;
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val view = `object` as View
        container.removeView(view)
    }
}
