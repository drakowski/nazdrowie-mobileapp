package www.restauracjanazdrowie.myapplication.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import www.restauracjanazdrowie.myapplication.R
import www.restauracjanazdrowie.myapplication.adapter.StartDataViewPagerAdapter
import www.restauracjanazdrowie.myapplication.model.Article

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [StartFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class StartFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    companion object {
        var mapFragment : SupportMapFragment?=null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view : View = inflater.inflate(R.layout.fragment_start, container, false);

        val viewPager : ViewPager = view.findViewById(R.id.startViewPage);

        val items : ArrayList<Article> = ArrayList();

        items.add(Article(resources.getString(R.string.start_welcome_title), R.drawable.banner11, resources.getString(R.string.start_welcome_body)));
        items.add(Article(resources.getString(R.string.start_restaurant_title), R.drawable.banner22,  resources.getString(R.string.start_restaurant_body)));
        items.add(Article(resources.getString(R.string.start_sales_title), R.drawable.banner33, resources.getString(R.string.start_sales_body)));

        val adapter : StartDataViewPagerAdapter? = context?.let { StartDataViewPagerAdapter(it, items) };

        viewPager.setAdapter(adapter);

        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        return view;
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        // Add a marker in Sydney and move the camera
        val sydney = LatLng(51.7633354, 19.4073445)
        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 17f))
    }
}