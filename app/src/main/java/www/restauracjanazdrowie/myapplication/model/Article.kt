package www.restauracjanazdrowie.myapplication.model

class Article {
    private var resourceId: Int;
    private var title: String;
    private var desc: String;

    constructor(title: String, resourceId: Int, desc: String) {
        this.resourceId = resourceId;
        this.title = title;
        this.desc = desc;
    }

    fun getResourceId(): Int {
        return resourceId;
    }

    fun getTitle(): String {
        return title;
    }

    fun getDesc(): String {
        return desc;
    }
}